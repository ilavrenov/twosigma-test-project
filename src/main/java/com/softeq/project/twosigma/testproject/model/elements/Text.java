/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.elements;

import com.softeq.project.twosigma.testproject.model.form.FormElementType;

/**
 *
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class Text extends BasicFormElement
{
    private String value;

    public Text(String value)
    {
        this.value = value;
    }

    @Override
    public FormElementType getFormElementType()
    {
        return FormElementType.TEXT;

    }

    @Override
    public boolean isEditable()
    {
        return false;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public void onAction()
    {
        //do nothing
    }

    @Override
    public void setValue(String value)
    {
        throw new UnsupportedOperationException("Text's value [label] is immutable");
    }

}
