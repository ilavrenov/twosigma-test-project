/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.elements;

import com.softeq.project.twosigma.testproject.model.form.FormElementType;

/**
 *
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class InputText extends BasicFormElement
{
    private static final String EMPTY_VALUE = "";

    private String value;

    public InputText()
    {
        this.value = EMPTY_VALUE;
    }

    @Override
    public FormElementType getFormElementType()
    {
        return FormElementType.INPUT;

    }

    @Override
    public boolean isEditable()
    {
        return true;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public void onAction()
    {
        System.out.println("New value after submit :" + this.value);
    }
}
