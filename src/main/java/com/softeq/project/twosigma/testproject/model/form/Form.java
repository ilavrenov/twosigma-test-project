/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.form;

import com.softeq.project.twosigma.testproject.model.elements.FormElement;
import com.softeq.project.twosigma.testproject.model.form.GridLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class Form implements Serializable
{

    private GridLayout gridLayout;

    private List<FormElement> elements = new ArrayList<>();

    public GridLayout getGridLayout()
    {
        return gridLayout;
    }

    public boolean hasElements()
    {
        return elements.isEmpty();
    }

    public List<FormElement> getElements()
    {
        return elements;
    }

    public Form setGridLayout(int rowsNumber, int columnsNumber)
    {
        gridLayout = new GridLayout(rowsNumber, columnsNumber);
        return this;
    }

    public Form addFormElement(FormElement formElement)
    {
        elements.add(formElement);
        return this;
    }

    public FormElement getFormElementById(String id)
    {
        FormElement foundElement = null;
        for (FormElement formElement : elements)
        {
            if (formElement.getId().equals(id))
            {
                foundElement = formElement;
            }
        }
        return foundElement;
    }

}
