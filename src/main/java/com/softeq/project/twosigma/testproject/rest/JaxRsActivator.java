/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
@ApplicationPath("/api")
public class JaxRsActivator extends Application
{
}
