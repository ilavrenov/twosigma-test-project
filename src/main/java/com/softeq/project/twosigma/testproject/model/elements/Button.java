/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.elements;

import com.softeq.project.twosigma.testproject.model.event.Event;
import com.softeq.project.twosigma.testproject.model.form.FormElementType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;

/**
 * <p/>
 * <p/>
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class Button extends BasicFormElement
{
    @JsonIgnore
    private List<Event> events = new ArrayList<>();

    private String value;

    public Button(String value)
    {
        this.value = value;
    }

    @Override
    public FormElementType getFormElementType()
    {
        return FormElementType.BUTTON;
    }

    @Override
    public boolean isEditable()
    {
        return false;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public void setValue(String value)
    {
        throw new UnsupportedOperationException("Button's value [label] is immutable");
    }

    @Override
    public void onAction()
    {
        for (Event event : events)
        {
            event.execute();
        }

    }

    public Button addEvent(Event event)
    {
        events.add(event);
        return this;

    }

    private Button deleteEvent(Event event)
    {
        events.remove(event);
        return this;
    }

}
