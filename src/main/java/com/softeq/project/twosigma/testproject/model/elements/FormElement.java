/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.elements;

import com.softeq.project.twosigma.testproject.model.form.FormElementType;

/**
 * <p/>
 * <p/>
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public interface FormElement<T>
{

    String getId();

    Position getPosition();

    FormElementType getFormElementType();

    boolean isEditable();

    T getValue();

    void setValue(T value);

    void onAction();
}
