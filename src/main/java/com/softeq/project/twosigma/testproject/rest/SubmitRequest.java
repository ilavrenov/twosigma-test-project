/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.rest;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class SubmitRequest implements Serializable
{
    private Map<String, String> submittedValues;

    private String buttonId;

    public Map<String, String> getSubmittedValues()
    {
        return submittedValues;
    }

    public void setSubmittedValues(Map<String, String> submittedValues)
    {
        this.submittedValues = submittedValues;
    }

    public String getButtonId()
    {
        return buttonId;
    }

    public void setButtonId(String buttonId)
    {
        this.buttonId = buttonId;
    }
}
