/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.event;

/**
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class PrintEvent implements Event
{
    @Override
    public void execute()
    {
        System.out.println("Element was pressed");

    }
}
