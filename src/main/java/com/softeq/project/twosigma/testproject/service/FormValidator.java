/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.service;

import com.softeq.project.twosigma.testproject.model.elements.FormElement;
import com.softeq.project.twosigma.testproject.model.form.Form;

import javax.enterprise.context.ApplicationScoped;

/**
 *
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
@ApplicationScoped
public class FormValidator
{

    public void validateFormElements(Form form)
    {

        for (FormElement formElement : form.getElements())
        {
            if (formElement.getPosition().getColumn() > form.getGridLayout().getColumns() - 1
                    || formElement.getPosition().getRow() > form.getGridLayout().getRows() - 1)
            {
                throw new IllegalArgumentException("Illegal number of element's position");
            }
        }

    }

}
