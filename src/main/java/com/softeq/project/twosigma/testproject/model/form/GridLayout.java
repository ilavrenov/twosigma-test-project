/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.form;

import java.io.Serializable;

/**
 * <p/>
 *
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public class GridLayout implements Serializable
{
    private int rows;

    private int columns;

    public GridLayout( int row,int column)
    {
        if (column < 0 || row < 0)
        {
            throw new IllegalArgumentException("Dimensions must be positive");
        }
        this.rows = row;
        this.columns = column;
    }

    public int getRows()
    {
        return rows;
    }

    public int getColumns()
    {
        return columns;
    }

}
