/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.model.elements;

import com.softeq.project.twosigma.testproject.model.form.FormElementType;
import java.io.Serializable;
import java.util.UUID;

/**
 * <p/>
 * <p/>
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
public abstract class BasicFormElement implements FormElement<String>, Serializable
{
    private String id;

    private Position position;

    public BasicFormElement()
    {
        this.id = UUID.randomUUID().toString();
    }

    public Position getPosition()
    {
        return position;
    }

    public BasicFormElement position(int row, int column)
    {
        position = new Position(row, column);
        return this;
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public abstract FormElementType getFormElementType();

    @Override
    public abstract boolean isEditable();

    @Override
    public abstract String getValue();

    @Override
    public abstract void setValue(String value);

    @Override
    public abstract void onAction();

}
