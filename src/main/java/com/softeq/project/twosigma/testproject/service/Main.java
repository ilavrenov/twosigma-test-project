/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.service;

import com.softeq.project.twosigma.testproject.model.elements.Button;
import com.softeq.project.twosigma.testproject.model.elements.InputText;
import com.softeq.project.twosigma.testproject.model.elements.Text;
import com.softeq.project.twosigma.testproject.model.event.PrintEvent;
import com.softeq.project.twosigma.testproject.model.form.Form;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * <p/>
 * <p/>
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
@ApplicationScoped
public class Main
{
    private Form form;

    @Inject
    private FormValidator formValidator;

    @PostConstruct
    private void init()
    {
        form = new Form();
        form.setGridLayout(3, 2);
        form.addFormElement(new Text("Name").position(0, 0));
        form.addFormElement(new Text("Surname").position(1, 0));
        form.addFormElement(new InputText().position(0, 1));
        form.addFormElement(new InputText().position(1, 1));
        form.addFormElement(new Button("Submit form").addEvent(new PrintEvent()).position(2, 1));

        formValidator.validateFormElements(form);

    }

    public Form getForm()
    {
        return form;
    }
}
