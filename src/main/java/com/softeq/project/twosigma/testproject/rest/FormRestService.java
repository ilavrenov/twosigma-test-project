/*
 * Copyright (c) 2015, test-project.
 * Developed by Softeq Development Corp. for TwoSigma.
 */

package com.softeq.project.twosigma.testproject.rest;

import com.softeq.project.twosigma.testproject.model.elements.FormElement;
import com.softeq.project.twosigma.testproject.service.Main;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * <p/>
 * <p/>
 * <p/>
 * Created on 28/05/2015.
 * <p/>
 *
 * @author Ivan Lavrenov
 */
@Path("/")
public class FormRestService
{
    @Inject
    private Main main;

    @GET
    @Path("/getform")
    @Produces("application/json;charset=utf-8")
    public Response getForm()
    {
        return Response.status(200).entity(main.getForm()).build();
    }

    @POST
    @Path("/submitform")
    @Consumes("application/json;charset=utf-8")
    @Produces("application/json;charset=utf-8")
    public Response submitForm(SubmitRequest submitRequest)
    {
        for (Map.Entry<String, String> entry : submitRequest.getSubmittedValues().entrySet())
        {
            FormElement updatedElement = main.getForm().getFormElementById(entry.getKey());
            if (updatedElement != null)
            {
                updatedElement.setValue(entry.getValue());
                updatedElement.onAction();
            }
        }

        FormElement formElement = main.getForm().getFormElementById(submitRequest.getButtonId());
        if (formElement != null)
        {
            formElement.onAction();
        }
        return Response.status(200).entity(Collections.singleton("Submit is successfully complited")).build();
    }
}