/**
 * Created by Aleksey Mitar on 5/28/2015.
 */
(function(exports) {
    function setId(element, id) {
        element.setAttribute("data-id", id);
    }

    function getId(element) {
        return element.getAttribute("data-id");
    }

    function getValue(element) {
        if(element.tagName === "INPUT") {
            return element.value;
        }
    }

    function cellPointer(row, column) {
        return "js-cell-" + row + "x" + column;
    }

    function createElementText(elementJson) {
        var element = document.createElement("span");
        element.className = "text";
        element.innerHTML = elementJson.value;
        setId(element, elementJson.id);
        return element;
    }

    function createElementInput(elementJson) {
        var element = document.createElement("input");
        element.type = "text"
        element.className = "input";
        element.value = elementJson.value;
        element.readOnly = !elementJson.editable;
        setId(element, elementJson.id);

        if(elementJson.editable) {
            element.className += " js-value-provider";
        }

        return element;
    }

    function createElementButton(elementJson) {
        var element = document.createElement("input");
        element.type = "button";
        element.className = "button";
        element.value = elementJson.value;
        setId(element, elementJson.id);
        return element;
    }

    function createElement(elementJson) {
        switch(elementJson.formElementType) {
            case "TEXT":
                return createElementText(elementJson);
            case "INPUT":
                return createElementInput(elementJson);
            case "BUTTON":
                return createElementButton(elementJson);
        }

        console.error("undefined type of element: ", elementJson.type);
    }

    function registerElementButton(form, element, elementJson) {
        element.addEventListener("click", function() {
            var values = form.querySelectorAll(".js-value-provider"),
                submittedValues = {},
                requestJson = {submittedValues: submittedValues},
                item,
                i;

            requestJson.buttonId = getId(element);
            for(i = 0; i < values.length; i++) {
                item = values.item(i);

                submittedValues[getId(item)] = getValue(item);
            }

            fetchJson(TwoSigma.ENDPOINTS.FORM_POST, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(requestJson)
            });
        }, false);
    }

    function registerElement(form, element, elementJson) {
        switch(elementJson.formElementType) {
            case "BUTTON":
                registerElementButton(form, element, elementJson);
                break;
        }
    }

    function createLayout(layoutJson) {
        var layout,
            row, cell,
            rows = layoutJson.rows,
            columns = layoutJson.columns,
            i, j;

        layout = document.createElement("div");
        layout.className = "l-grid l-grid-" + columns;

        for(var i = 0; i < rows; i++) {
            row = document.createElement("div");
            row.className = "l-row";
            layout.appendChild(row);

            for(var j = 0; j < columns; j++) {
                cell = document.createElement("div");
                cell.className = "l-cell " + cellPointer(i, j);
                row.appendChild(cell);
            }
        }

        return layout;
    }

    function createForm(formJson) {
        var form,
            table;

        form = document.createElement("form");
        form.className = "l-form";

        table = createLayout(formJson.gridLayout);
        form.appendChild(table);

        formJson.elements.forEach(function(elementJson) {
            var element = createElement(elementJson),
                position = elementJson.position,
                site = table.querySelector("." + cellPointer(position.row, position.column));

            if(site && element) {
                site.appendChild(element);

                registerElement(form, element, elementJson);
            }
        });

        return form;
    }

    var FETCH = {
        success: function(response) {
            if(response.status >= 200 && response.status < 400) {
                return Promise.resolve(response);
            }
            else {
                return Promise.reject(new Error(response.statusText));
            }
        },
        json: function(response) {
            return response.json();
        }
    };

    function fetchJson() {
        return fetch.apply(null, arguments).then(FETCH.success).then(FETCH.json);
    }

    function getFormContent() {
        return fetchJson(TwoSigma.ENDPOINTS.FORM_GET)
    }

    function initialize(viewport) {
        getFormContent().then(function(formJson) {
            var form = createForm(formJson);
            viewport.appendChild(form);
        });
    }

    exports.init = initialize;

})((function() {
    var exports = window.TwoSigma;
    if(exports == null) {
        window.TwoSigma = exports = {};
    }
    return exports;
})());