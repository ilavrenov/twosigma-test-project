<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <meta charset="UTF-8">
  <title>Twosigma Test Task</title>
  <link rel="stylesheet" href="css/form.css">
  <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
</head>
<body>
<h3 class="title">Form:</h3>
<div id="result"></div>
<script>
  TwoSigma = {
    "ENDPOINTS": {
      "FORM_GET": "${contextPath}/api/getform",
      "FORM_POST": "${contextPath}/api/submitform"
    }
  };
</script>
<script type="text/javascript" src="js/vendor/Promise.js"></script>
<script type="text/javascript" src="js/vendor/fetch.js"></script>
<script type="text/javascript" src="js/form.js"></script>
<script>
  TwoSigma.init(document.getElementById("result"));
</script>
</body>
</html>